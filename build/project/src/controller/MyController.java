package controller;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

public class MyController implements Initializable {

    @FXML
    private Button startBtn;

    @FXML
    private Button stopBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    private static ChromeDriver driver;
    private static ChromeOptions options;

    public void startSelenium(ActionEvent event) {
        driver = this.initDriver();
        driver.get("http://docs.oracle.com/javafx/2/get_started/fxml_tutorial.htm");
    }

    public void stopSelenium(ActionEvent event) {
        driver.quit();
    }

    public ChromeDriver initDriver() {
        System.setProperty("webdriver.chrome.driver", "d:/driver/chromedriver.exe");
        options = new ChromeOptions();
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        // disable auto save
        chromePrefs.put("credentials_enable_service", false);
        chromePrefs.put("profile.password_manager_enabled", false);
        options.setExperimentalOption("prefs", chromePrefs);
        // maximum win down
        options.addArguments("--start-maximized");
        // private mode
        options.addArguments("--incognito");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(options);
        return driver;
    }

}
