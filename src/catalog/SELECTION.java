package catalog;

import java.util.HashMap;
import java.util.Map;

public enum SELECTION {

    DRAW("x"),
    HOME("1"),
    AWAY("2"),
    OVER("Over"),
    UNDER("Under"),
    H("h"),
    A("a")
    ;

    private SELECTION(String v) {
        this.v = v;
    }

    private static Map<String, SELECTION> vMap = new HashMap<String, SELECTION>();
    static {
        for (SELECTION r : SELECTION.values()) {
            vMap.put(r.v, r);
        }
    }

    public static SELECTION fromV(String v){
        return vMap.get(v);
    }

    public String v;

    public String getV() {
        return this.v;
    }

}
