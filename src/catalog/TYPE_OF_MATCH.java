package catalog;

import java.util.HashMap;
import java.util.Map;

public enum TYPE_OF_MATCH {

    ASIA_1X2("Asian 1X2"),
    HDP("Handicap"),
    OU("Over/Under"),
    EURO_1X2("1X2"),
    TOTAL_GOAL("Total Goal")
    ;

    public String v;

    private TYPE_OF_MATCH(String v) {
        this.v = v;
    }

    private static Map<String, TYPE_OF_MATCH> vMap = new HashMap<String, TYPE_OF_MATCH>();
    static {
        for (TYPE_OF_MATCH r : TYPE_OF_MATCH.values()) {
            vMap.put(r.v, r);
        }
    }

    public static TYPE_OF_MATCH fromV(String v) {
        return vMap.get(v);
    }

    public String getV() {
        return this.v;
    }

}
