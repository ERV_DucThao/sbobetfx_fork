package utils;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.http.HttpStatus;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;

public class JsoupUtils {

    public static String getLink(String str) throws Exception {
        try {
            Document document = Jsoup.connect("http://keobong88.com/").get();
            Elements allStrong = document.getElementsByTag("strong");
            int index = IntStream.range(0, allStrong.size()).filter(i -> allStrong.get(i).ownText().contains(str)).findFirst().getAsInt();
            Elements oldList = allStrong.get(index + 1).getElementsByTag("a");
            List<String> newList = oldList.stream().map(href -> href.attr("href")).collect(Collectors.toList());
            for (String href : newList) {
                Response response = Jsoup.connect(href).timeout(2000).method(Method.GET).execute();
                if (response.statusCode() == HttpStatus.SC_OK) {
                    return href;
                }
            }
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("[TAG NOT FOUND: " + str + " ]");
        } catch (Exception e) {
        }
        throw new Exception("[CAN'T FIND LINK TO CONNECT FOR: " + str);
    }

    public static Document clean(ChromeDriver driver) {
        Document document = Jsoup.parse(driver.getPageSource());
        Jsoup.clean(document.outerHtml(), Whitelist.simpleText());
        document.html(document.outerHtml().replace("\u200c", "").replace("&zwnj;", ""));
        return document;
    }

}
