package action;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Uninterruptibles;

import vn.com.football.catalog.BET_TYPES;
import vn.com.football.catalog.SELECTION;
import vn.com.football.catalog.TYPE_OF_MATCH;
import vn.com.football.dto.BetDto;
import vn.com.football.utils.JsoupUtils;
import vn.com.football.utils.SeleniumUtils;

public class Dad {

    private final String USERNAME = "rqplt005";
    private final String PASSWORD = "tttt2828@";
    private final String PASSCODE = "123123";
    private final String C_USERNAME = "m01d00ks666";
    private final String C_PASSWORD = "pppp8888@";
    private final String TARGET = "rqplt05001";
    private final int WAIT_TIME = 2;
    private final int MONEY_BET = 10;
    // list chứa matchId để kiểm tra có cần add lại hay không
    private List<String> isBetted = new ArrayList<>();

    @Test
    public void testSbobet() throws Exception {
        ChromeDriver driver = SeleniumUtils.initDriver();
        WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME);
        driver.get(JsoupUtils.getLink("Sbo"));
        login(wait, C_USERNAME, C_PASSWORD);
        driver.executeScript("$('.sign-in').click();");
        driver.executeScript("$('#lang-menu > li > a:contains(English)').click();");
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        driver.executeScript("window.open('about:blank', '_blank');");
        List<String> tabList = Lists.newArrayList(driver.getWindowHandles());
        String playerTab = tabList.get(0);
        String managerTab = tabList.get(1);
        driver.switchTo().window(managerTab);
        driver.get(JsoupUtils.getLink("Agent Sbo"));
        login(wait, USERNAME, PASSWORD);
        new Select(driver.findElementById("lang")).selectByIndex(0);
        driver.findElementById("btnSubmit").submit();
        //Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        secureCode(driver);
        //int count = 0;
        //int maxOfBet = 5;
        while(true) {
            try {
                refreshData(driver);
                WebDriver rightFrame = driver.switchTo().frame("RightFrame");
                rightFrame.findElement(By.linkText(TARGET)).click();;
                Document document = Jsoup.parse(rightFrame.getPageSource());
                List<BetDto> listA = getDataForBet(document);
                driver.switchTo().window(playerTab);
                Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
                //List<BetDto> listB = isBetInChild(driver);
                Document html = JsoupUtils.clean(driver);
                for (BetDto betDtoA : listA) {
                    if (isBetted.contains(betDtoA.matchId)) continue;
                    childBet(html, betDtoA, driver);
                    //count++;
                }
                driver.switchTo().window(managerTab);
                listA.clear();
                //if (count == maxOfBet) break;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                isBetted.clear();
                //driver.quit();
            }
        }
    }

    public List<BetDto> isBetInChild(ChromeDriver driver) {
        List<BetDto> listB = new ArrayList<>();
        driver.executeScript("$('#my-bets').click();");
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        List<String> tabList = Lists.newArrayList(driver.getWindowHandles());
        String parentTab = tabList.get(0);
        String betsTab = tabList.get(2);
        driver.switchTo().window(betsTab);
        List<WebElement> trs = driver.findElementsByCssSelector(".ContentTable.SingleRow > tbody > tr");
        for (WebElement tr : trs) {
            if(tr.getAttribute("onmouseout") != null && tr.getAttribute("onmouseover") != null) {
                if (tr.findElements(By.cssSelector("td.Font12")).get(1).getText().contains("Running")) {
                    BetDto betDto = new BetDto();
                    WebElement details = tr.findElement(By.cssSelector("td:nth-child(2) > span.FontNormal")); // Details
                    betDto.betTime = this.getBetTime(details.getText().replace("\n", " "));
                    WebElement selection = tr.findElements(By.cssSelector("td.AR")).get(0); // Selection
                    betDto.home = StringUtils.substringBefore(selection.findElement(By.cssSelector("span.FontRed.Font12")).getText(), "@Live"); // home
                    betDto.away = StringUtils.substringAfter(selection.findElement(By.cssSelector("span.Font12.Block")).getText(), "-vs- "); // away
                    betDto.typeOfBet = typeOfBet(selection.findElement(By.cssSelector("span.FontMidBlue.FontNormal.Block")).getText()); // type of bet
                    betDto.typeOfMatch = typeOfMatch(selection.findElement(By.cssSelector("span.FontMidBlue.FontNormal.Block")).getText()); // type of math
                    listB.add(betDto);
                }
            }
        }
        driver.close();
        driver.switchTo().window(parentTab);
        return listB;
    }

    private void childBet(Document html, BetDto bet, ChromeDriver driver) {
        switch (bet.typeOfBet) {
        case HALF_TIME:
        case FULL_TIME:
            betForMatch(html, bet, driver);
        case NEXT_GOAL:
            break;
        }
    }

    private void betForMatch(Document html, BetDto bet, ChromeDriver driver) {
        switch (bet.typeOfMatch) {
        case ASIA_1X2:
            break;
        case HDP:
            betHdp(html, bet, driver);
            break;
        case OU:
            betOu(html, bet, driver);
            break;
        case EURO_1X2:
            betEuro(html, bet, driver);
            break;
        case TOTAL_GOAL:
            break;
        }
    }

    private void betEuro(Document html, BetDto bet, ChromeDriver driver) {
        String home = bet.home;
        int halfOrAllMatch = bet.typeOfBet.getV();
        String spanStr = "span:contains(" + home + ")";
        Element span = html.select(spanStr).first();
        if (span != null) {
            Element tr = span.parent().parent();
            Elements spans = tr.select(".BBN.BRN > a[class*=odds-wrap] > span[id*=':1:']");
            String id = spans.get(halfOrAllMatch).attr("id");
            String matchId = Splitter.on(":").splitToList(id).get(3); // bu:od:price:31658943:2:5.00:1
            String idClick = Joiner.on("").join("$('span[id*=\"", matchId, ":1", ":\"]').click();");
            driver.executeScript(idClick);
            SeleniumUtils.acceptAlert(driver);
            bet(driver);
        }
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
    }

    private void betOu(Document html, BetDto bet, ChromeDriver driver) {
        String home = bet.home;
        String homeOrAway = "";
        if (bet.selection.equals(SELECTION.OVER)) {
            homeOrAway = SELECTION.H.getV();
        } else {
            homeOrAway = SELECTION.A.getV();
        }
        int halfOrAllMatch = bet.typeOfBet.getV();
        String spanStr = "span:contains(" + home + ")";
        Element span = html.select(spanStr).first();
        if (span != null) {
            Element tr = span.parent().parent();
            Elements spans = tr.select(".BBN.BRN > a[class*=odds-ou] span[id^=bu]");
            String id = spans.get(halfOrAllMatch).attr("id");
            String matchId = Splitter.on(":").splitToList(id).get(3); // bu:od:price:31113137:h:0.97:2
            String idClick = Joiner.on("").join("$('span[id*=\"", matchId, ":", homeOrAway, "\"]').click();");
            driver.executeScript(idClick);
            SeleniumUtils.acceptAlert(driver);
            bet(driver);
        }
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
    }

    private void bet(ChromeDriver driver) {
        // Add money bet
        String sendMoneyBet = "$('.stake-input').val('" + MONEY_BET + "');";
        driver.executeScript(sendMoneyBet);
        driver.executeScript("$('input[id*=submittc]').click();");
        if (SeleniumUtils.isAlertPresent(driver)) {
            if (driver.switchTo().alert().getText().contains("Your stake cannot be lower than the minimum bet.")) {
                SeleniumUtils.acceptAlert(driver);
                driver.executeScript("$('input[id*=submittc]').click();");
                SeleniumUtils.acceptAlert(driver);
            } else {
                SeleniumUtils.acceptAlert(driver);
                SeleniumUtils.acceptAlert(driver);
                SeleniumUtils.acceptAlert(driver);
            }
        }
    }

    public void betHdp(Document html, BetDto bet, ChromeDriver driver) {
        String home = bet.home;
        String homeOrAway = bet.selection.getV();
        int halfOrAllMatch = bet.typeOfBet.getV();
        String spanStr = "span:contains(" + home + ")";
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        Element span = html.select(spanStr).first();
        if (span != null) {
            Element tbodyFirst = span.parent().parent().parent();
            Element tbodySecond = null;
            Element tbodyThird = null;
            boolean isExisted = tbodyFirst.nextElementSibling().cssSelector().contains("subrow");
            if(isExisted) tbodySecond = tbodyFirst.nextElementSibling();
            isExisted = tbodySecond.nextElementSibling().cssSelector().contains("subrow");
            if(isExisted) tbodyThird = tbodySecond.nextElementSibling();
            Elements spans = getSpans(tbodyFirst, tbodySecond, tbodyThird, bet.hdpPoint);
            String id = spans.get(halfOrAllMatch).attr("id");
            String matchId = Splitter.on(":").splitToList(id).get(3);
            String idClick = Joiner.on("").join("$('span[id*=\"", matchId, ":", homeOrAway, "\"]').click();");
            driver.executeScript(idClick);
            Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
            SeleniumUtils.acceptAlert(driver);
            bet(driver);
        }
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        isBetted.add(bet.matchId);
    }

    public Elements getSpans1(Element tbodyFirst, Element tbodySecond, Element tbodyThird, String hdpPoint) {
        String cssSelector = ".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + hdpPoint + ") + span span[id^=bu]";
        Elements spans = tbodyFirst.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + hdpPoint + ") + span span[id^=bu]");
        //Elements spans = tbodyFirst.select(cssSelector);
        if (spans.size() != 0)
            return spans;
        if (tbodySecond != null) {
            spans = tbodySecond.select(cssSelector);
            if (spans.size() != 0)
                return spans;
        }
        if (tbodyThird != null) {
            spans = tbodyThird.select(cssSelector);
            if (spans.size() != 0)
                return spans;
        }
        spans = tbodyFirst.child(0).select(".BBN.BRN > a[class*=odds-hdp] span[id^=bu]");
        return spans;
    }

    private static Elements getSpans(Element tbody, Element tbodyNext, Element tbodyThree, String rate){

        Elements spans = tbody.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + rate + ") + span span[id^=bu]");
        if(spans.size() != 0) return spans;

        if(tbodyNext != null){
            spans = tbodyNext.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + rate + ") + span span[id^=bu]");
            if(spans.size() != 0) return spans;
        }

        if(tbodyThree != null){
            spans = tbodyThree.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + rate + ") + span span[id^=bu]");
            if(spans.size() != 0) return spans;
        }

        spans = tbody.child(0).select(".BBN.BRN > a[class*=odds-hdp] span[id^=bu]");
        return spans;

    }

    private List<BetDto> getDataForBet(Document document) {
        List<BetDto> betList = new ArrayList<>();
        Elements trs = document.select("#divDataList > table > tbody > tr");
        for (Element tr : trs) {
            if (tr.select("td.TAC").get(3).text().trim().equals("Running")) {
                Element td = tr.select("td.TAR.VAT").first();
                Elements spans = td.getElementsByTag("span");
                String select = spans.first().text();
                String home = spans.get(6).text();
                String away = spans.get(7).text();
                String str = td.select(".WSN > .FSI").first().text();
                BET_TYPES typeOfBet = typeOfBet(str);
                TYPE_OF_MATCH typeOfMatch = typeOfMatch(str);
                BetDto child = new BetDto();
                child.home = home;
                child.away = away;
                child.typeOfBet = typeOfBet;
                child.typeOfMatch = typeOfMatch;
                child.selection = selection(typeOfMatch, select, home, away);
                child.league = td.select(".CF60").first().text().trim() + "@" + td.select(".CF60").last().text().trim();
                // id của trận đấu
                child.matchId = tr.select("td.TAC > span.DeepBlue.FW700").first().text().replace("i", "").trim();
                child.hdpPoint = hdpPoint(spans.select(".DeepBlue.FW700").text());
                // thời gian đánh chính xác để so sánh
                child.betTime = getBetTime(StringUtils.substringAfter(tr.select("td.TAC").get(1).text(), "Football "));
                betList.add(child);
                //isBetted.add(tr.select("td.TAC > span.DeepBlue.FW700").first().text().replace("i", "").trim());
            }
        }
        return betList;
    }

    public DateTime getBetTime(String str) {
        return DateTimeFormat.forPattern("MM/dd/yyyy hh:mm:ss a").parseDateTime(str);
    }

    public String hdpPoint(String str) {
        switch (str) {
        case "0.00":
            return "0.0";
        case "0.25":
            return "0-0.5";
        case "0.50":
            return "0.50";
        case "0.75":
            return "0.5-1";
        case "1.00":
            return "1.0";
        case "1.25":
            return "1-1.5";
        case "1.5":
            return "1.50";
        case "1.75":
            return "1.5-2";
        case "2":
            return "2.0";
        default:
            return "";
        }
    }

    public BET_TYPES typeOfBet(String str) {
        if (str.contains("First Half")) {
            return BET_TYPES.HALF_TIME;
        }
        if (str.contains("Next Goal")) {
            return BET_TYPES.NEXT_GOAL;
        }
        return BET_TYPES.FULL_TIME;
    }

    // only apply for FULL_TIME and HALF_TIME
    private TYPE_OF_MATCH typeOfMatch(String str) {
        if (str.contains("First Half Hdp")) {
            return TYPE_OF_MATCH.HDP;
        }
        if (str.contains("First Half O/U")) {
            return TYPE_OF_MATCH.OU;
        }
        if (str.contains("First Half 1X2")) {
            return TYPE_OF_MATCH.EURO_1X2;
        }
        return TYPE_OF_MATCH.fromV(str);
    }

    // only apply for FULL_TIME and HALF_TIME
    public SELECTION selection(TYPE_OF_MATCH typeOfMatch, String select, String home, String away) {
        switch (typeOfMatch) {
        case OU:
        case EURO_1X2:
            return SELECTION.fromV(select);
        case ASIA_1X2:
        case HDP:
            if (select.contains(home)) {
                return SELECTION.H;
            }
            if (select.contains(away)) {
                return SELECTION.A;
            }
        case TOTAL_GOAL:
            break; // chưa làm
        }
        return null;
    }

    public boolean refreshData (ChromeDriver driver) {
        boolean hasRecord = false;
        while (true) {
            // bấm vào 1.17
            driver.executeScript("document.getElementById('MenuFrame').contentWindow.document.getElementById('TotalBet2_Agent_Member').onclick();");
            if (SeleniumUtils.isAlertPresent(driver)) {
                driver.switchTo().alert().accept();
            } else {
                hasRecord = true;
                break;
            }
            // đợi 3s bấm nút refresh một lần
            Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
            driver.executeScript("document.getElementById('RightFrame').contentWindow.document.getElementById('btnRefresh').onclick();");
            if (SeleniumUtils.isAlertPresent(driver)) {
                driver.switchTo().alert().accept();
            } else {
                hasRecord = true;
                break;
            }
        };
        return hasRecord;
    }

    private String passCode(int index) {
        return String.valueOf(PASSCODE.charAt(index - 1));
    }

    private void secureCode(ChromeDriver driver) {
        if (driver.getPageSource().contains("firstposition")) {
            int firstNum = NumberUtils.toInt(driver.findElementById("firstposition").getText().substring(0, 1));
            int lastNum = NumberUtils.toInt(driver.findElementById("secondposition").getText().substring(0, 1));
            driver.findElementById("FirstChar").sendKeys(passCode(firstNum));
            driver.findElementById("SecondChar").sendKeys(passCode(lastNum));
            driver.findElementById("btnSubmit").submit();
        } else { // IP has been accepted
            driver.executeScript("alert('Wellcome to SBOBET manager page!!!')");
            driver.switchTo().alert().accept();
        }
    }

    private void login(WebDriverWait wait, String userName, String passWord) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username"))).sendKeys(userName);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password"))).sendKeys(passWord);
    }

}
