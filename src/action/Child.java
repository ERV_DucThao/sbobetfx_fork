package action;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.http.HttpStatus;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.util.concurrent.Uninterruptibles;

import vn.com.football.utils.SeleniumUtils;

public class Child {

    //private final static String USER_NAME = "vnjav2";
    //private final static String PASS_WORD = "123kh0ngco";
    private final static String USER_NAME = "m01d00ks666";
    private final static String PASS_WORD = "pppp8888@";
    private final static int WAIT_TIME_BY_SECONDS = 2;
    private final static int MONEY_BET = 10;

    public static void main(String... strings) throws Exception {
        ChromeDriver driver = SeleniumUtils.initDriver();
        try {
            WebDriverWait wait = new WebDriverWait(driver, WAIT_TIME_BY_SECONDS);
            String linkSbobet = getLink("Sbo");
            driver.get(linkSbobet);
            login(wait, USER_NAME, PASS_WORD);
            driver.executeScript("$('.sign-in').click();");
            //driver.findElementById("depositLaterBtn").click();
            //String home = "B‌‌os‌‌n‌‌‌‌‌‌i‌‌‌a‌‌-‌‌‌‌‌H‌‌erzegovina (w)";
            String home = "Neman Grodno";
            String homeOrAway = "h";
            int halfOrAllMatch = 0;
            int type = 0; // 0: hdp, 1: ou, 2: 1x2
            String _1x2 = "1";
            String rate = "0.50";

            WebElement webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("module-odds-display")));
            System.out.println(webElement);
            //driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            //Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);

            // Get all htmls
            Document document = Jsoup.parse(driver.getPageSource());
            Jsoup.clean(document.outerHtml(), Whitelist.simpleText());
            //FileUtils.writeStringToFile(new File("driver/output.html"), document.outerHtml(), "UTF-8");
            String htmls = document.outerHtml().replace("\u200c", "").replace("&zwnj;", "");
            document.html(htmls);

            // Clean the document.
            //document = new Cleaner(Whitelist.simpleText()).clean(document);

            // Adjust escape mode
            //document.outputSettings().escapeMode(EscapeMode.xhtml);

            // HDP Cuoc co keo
            String spanStr = "span:contains(" + home + ")";
            Element span = document.select(spanStr).first();
            Element tbody = span.parent().parent().parent();
            Element tbodyNext = null;
            Element tbodyThree = null;
            boolean isExisted = tbody.nextElementSibling().cssSelector().contains("subrow");
            if(isExisted) tbodyNext = tbody.nextElementSibling();
            isExisted = tbodyNext.nextElementSibling().cssSelector().contains("subrow");
            if(isExisted) tbodyThree = tbodyNext.nextElementSibling();
            //FileUtils.writeStringToFile(new File("driver/output.html"), tbody.outerHtml(), "UTF-8");

            Elements spans = getSpans(tbody, tbodyNext, tbodyThree, rate);

            String id = "";
            /*

            Elements spans = null;
            if(type == 0){
                spans = tr.select(".BBN.BRN > a[class*=odds-hdp] span[id^=bu]");
            }else if(type == 1){
                spans = tr.select(".BBN.BRN > a[class*=odds-ou] span[id^=bu]");
            }else{
                spans = tr.select(".BBN.BRN > a[class*=odds-wrap] span[id*=':1:']");
            }
            */

            id = spans.get(halfOrAllMatch).attr("id");


            String numHDP = Splitter.on(":").splitToList(id).get(3); // bu:od:price:31113137:h:0.97:2
            //String idClick = "$('span[id*=\"" + numHDP + ":" + homeOrAway + "\"]').click();";
            String idClick = "";
            if(type == 2){
                idClick = Joiner.on("").join("$('span[id*=\"", numHDP, ":", _1x2, ":\"]').click();"); // other way
            }else{
                idClick = Joiner.on("").join("$('span[id*=\"", numHDP, ":", homeOrAway, "\"]').click();"); // other way
            }
            driver.executeScript(idClick);

            // Turn off popup
            try {
                Alert alert = driver.switchTo().alert();
                if(alert != null) driver.switchTo().alert().dismiss();
            } catch (NoAlertPresentException e) {}

            // Add money bet
            String sendMoneyBet = "$('.stake-input').val('" + MONEY_BET + "');";
            driver.executeScript(sendMoneyBet);

            // Submit
            driver.executeScript("$('input[id*=canceltc]').click();");
            //driver.executeScript("$('input[id*=submittc]').click();");

            Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);

            driver.quit();
        } catch (Exception e) {
            e.printStackTrace();
            driver.quit();
        }
    }

    private static Elements getSpans(Element tbody, Element tbodyNext, Element tbodyThree, String rate){

        Elements spans = tbody.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + rate + ") + span span[id^=bu]");
        if(spans.size() != 0) return spans;

        if(tbodyNext != null){
            spans = tbodyNext.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + rate + ") + span span[id^=bu]");
            if(spans.size() != 0) return spans;
        }

        if(tbodyThree != null){
            spans = tbodyThree.select(".BBN.BRN > a[class*=odds-hdp] > span[class*=hdp-point]:contains(" + rate + ") + span span[id^=bu]");
            if(spans.size() != 0) return spans;
        }

        spans = tbody.child(0).select(".BBN.BRN > a[class*=odds-hdp] span[id^=bu]");
        return spans;

    }

    private static void login(WebDriverWait wait, String userName, String passWord) {
        WebElement uName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
        uName.clear();
        uName.sendKeys(userName);
        WebElement pass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
        pass.clear();
        pass.sendKeys(passWord);
    }

    private static String getLink(String str) throws Exception {
        try {
            Document document = Jsoup.connect("http://keobong88.com/").get();
            Elements allStrong = document.getElementsByTag("strong");
            int index = IntStream.range(0, allStrong.size()).filter(i -> allStrong.get(i).ownText().contains(str)).findFirst().getAsInt();
            Elements oldList = allStrong.get(index + 1).getElementsByTag("a");
            List<String> newList = oldList.stream().map(href -> href.attr("href")).collect(Collectors.toList());
            for (String href : newList) {
                Response response = Jsoup.connect(href).timeout(2000).method(Method.GET).execute();
                if (response.statusCode() == HttpStatus.SC_OK) {
                    return href;
                }
            }
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("[TAG NOT FOUND: " + str + " ]");
        } catch (Exception e) {
            e.printStackTrace();
        }
        throw new Exception("[CAN'T FIND LINK TO CONNECT FOR: " + str);
    }


}
