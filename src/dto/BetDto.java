package dto;

import java.io.Serializable;

import org.joda.time.DateTime;

import vn.com.football.catalog.MATCH_MODE;
import vn.com.football.catalog.SELECTION;
import vn.com.football.catalog.BET_TYPES;
import vn.com.football.catalog.TYPE_OF_MATCH;

public class BetDto implements Serializable {

    private static final long serialVersionUID = -5520316067297777417L;

    public SELECTION selection;
    public TYPE_OF_MATCH typeOfMatch;
    public BET_TYPES typeOfBet;
    public MATCH_MODE matchMode;
    public String home;
    public String away;
    public String league;
    public String matchId;
    public String hdpPoint;
    public DateTime betTime;

}
